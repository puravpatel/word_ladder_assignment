import unittest
from word_ladder import same, build, find



class TestLadder(unittest.TestCase):


    def test_same(self):



        self.assertEqual(same("hide", "seek"), 0)
        self.assertEqual(same("lead", "gold"), 1)
        self.assertEqual(same("make", "mute"), 2)
        self.assertEqual(same("desk", "book"), 1)




    def test_find(self):
        result8 = find("hide", "dictionary.txt", False, "seek", True )
        result9 = find("lead", "dictionary.txt", False, "gold", True)
        result10 = find("hide", "dictionary.txt", False, "seek", True)
        self.assertEqual(result8, False)
        self.assertEqual(result9, False)
        self.assertEqual(result10, False)

    def test_build(self):
        queue = []
        for i in range(len("lead")):
            queue += build(".ead", "dictionary.txt['lead, 'load', 'boad']", False, queue)
        queue2 = []
        for i in range(len("gold")):
            queue2 = build(".old", "dictionary.txt['gold', 'goad', 'read']", False, queue)
        queue3 = []
        for i in range(len("hide")):
            queue3 = build(".ide", "dictionary.txt['ride', 'bide', 'side')", False, queue)
        #print(queue)
        self.assertFalse(queue, False)
        self.assertFalse(queue2, False)
        self.assertFalse(queue3, False)




if __name__ == '__main__':
    unittest.main()